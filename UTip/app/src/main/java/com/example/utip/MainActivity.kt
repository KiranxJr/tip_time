package com.example.utip

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.utip.databinding.ActivityMainBinding
import java.text.NumberFormat
import kotlin.math.ceil

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.calBtn.setOnClickListener { CalculateTip() }
    }

    fun CalculateTip() {
        val stringInTextField = binding.costOfServiceEditText.text.toString()
        val costVal = stringInTextField.toDouble()
        val selectedTip = binding.tipOptions.checkedRadioButtonId
        val tipPrecentage = when (selectedTip) {
            R.id.twenty_per -> 0.20
            R.id.eighteen_per -> 0.18
            else -> 0.15

        }
        var tip = tipPrecentage * costVal
        var roundUp = binding.tipRound.isChecked
        if (roundUp) {
            tip = ceil(tip)
        }
        val formattedTip = NumberFormat.getCurrencyInstance().format(tip)
        binding.result.text = getString(R.string.tip_amount, formattedTip)
    }


}